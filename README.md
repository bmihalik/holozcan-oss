# HoloZcan OSS #

This is repository of HoloZcan Open Source System. HoloZcan is a project name of a holographic microscope based analysis system. The system itself has wide scope and covers applications for security and defense purpose. Part of the project is to increase preparedness of society for disasters related to biological threats.

This repository contains basic hardware controlling, data processing and data conversion tools for holographic microscope. 

### Used resources ###

In our initial work we used the folowing resources:

1. DIHM 3D printable files:
https://github.com/teph12/DIHM 

2. Article about DIY DIHM:
https://doi.org/10.1038/s41598-019-47689-1

3. Tutorial about holographic simulations:
http://kmdouglass.github.io/posts/simulating-inline-holograms/

4. Article about reconstruction algorithms:
https://doi.org/10.1364/AO.54.002424

### QuckStart ###

1. Construct microscope using pars: doc/Teph12_DIHM_Construction.txt
2. Copy codes to Raspberry Pi.
3. Run src/control/dihm-takepic.py
4. Run src/recon/dihm-recon2.py

### Developer ###

Bela Mihalik <bela.mihalik@gmail.com>
