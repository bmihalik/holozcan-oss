#!/usr/bin/python3

import os
import sys
import pylab
import imageio

import numpy as np
import holopy as hp

from holopy.propagation import ps_propagate
from scipy.ndimage.measurements import center_of_mass
from holopy.core.io import get_example_data_path
from holopy.core.process import bg_correct

imagepath = "/data/workplace/ideas/holozcan/teph12-DIHM/pics/2020-04-10_01:55:56-targylemez/object.h5"
bgpath = "/data/workplace/ideas/holozcan/teph12-DIHM/pics/2020-04-10_01:57:13-hatter/object.h5"

raw_holo = hp.load( imagepath )
bg = hp.load( bgpath ) + 100.0

#holo = raw_holo
#holo = bg_correct( raw_holo, bg )
holo = raw_holo.copy()
holo.values = raw_holo.values / bg.values

print( "xlen = " + str(len(holo.x)) )
print( "ylen = " + str(len(holo.y)) )

zstack = np.array([5910.0]) # distances from camera to reconstruct
print( "zstack = " + str(zstack) )

#beam_c = center_of_mass(bg.values.squeeze()) # get beam center
#print( "beam_c = " + str(beam_c) )

w = len(holo.x)
h = len(holo.y)
win = 2048
x1 = int( w/2 - win/2 )
x2 = int( x1 + win )
y1 = int( h/2 - win/2 )
y2 = int( y1 + win )

holo2 = holo.isel( x=slice(x1,x2), y=slice(y1,y2) )

recons = hp.propagate( holo, zstack, cfsp = 10 )

imageio.imwrite( "object-amplitude.png", np.absolute(recons.isel(z=0).values) )
imageio.imwrite( "object-phase.png", np.angle(recons.isel(z=0).values) )

recons.values = recons.values.astype('complex64')
hp.save( "object.h5", recons )

pylab.figure(1)
pylab.imshow( np.absolute(recons.isel(z=0).values), cmap='gray' )
pylab.show()

