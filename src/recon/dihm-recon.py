#!/usr/bin/python3
#
# Point Source based reconstruction - works only for sparse objects
#

import os
import sys

import pylab

import numpy as np
import holopy as hp

from holopy.propagation import ps_propagate
from scipy.ndimage.measurements import center_of_mass
from holopy.core.io import get_example_data_path
from holopy.core.process import bg_correct

imagepath = "/data/workplace/ideas/holozcan/teph12-DIHM/pics/2020-04-04_20:54:47-targylemez/object0-crop.jpg"
bgpath = "/data/workplace/ideas/holozcan/teph12-DIHM/pics/2020-04-04_20:45:12-bg/background0-crop.jpg"

#imagepath = get_example_data_path('ps_image01.jpg')
#bgpath = get_example_data_path('ps_bg01.jpg')

L = 30e-3 # distance from light source to screen/camera
cam_spacing = 1.1e-6 # linear size of camera pixels
mag = 9.0 # magnification
npix_out = 512 # linear size of output image (pixels)
wavelen = 430e-9

#zstack = np.arange(5.908e-3, 5.912e-3, wavelen)  # distances from camera to reconstruct
zstack = np.arange(1.08e-3, 1.18e-3, 0.01e-3) # distances from camera to reconstruct

print( "zstack = "  + str(zstack) )

holo = hp.load_image(imagepath, spacing=cam_spacing, illum_wavelen=wavelen, medium_index=1.0,channel=2) # load hologram
bg = hp.load_image(bgpath, spacing=cam_spacing,channel=2) # load background image
cholo = hp.core.process.bg_correct(holo, bg) # divide hologram with background

beam_c = center_of_mass(bg.values.squeeze()) # get beam center
print( "beam_c = " + str(beam_c) )

#w = len(holo.x)
#h = len(holo.y)
#win = 100
#x1 = int( w/2 - win/2 )
#x2 = int( x1 + win )
#y1 = int( h/2 - win/2 )
#y2 = int( y1 + win )

#holo2 = holo.isel( z=0, x=slice(x1,x2), y=slice(y1,y2) )
#bg2 = bg.isel( z=0, x=slice(x1,x2), y=slice(y1,y2) )
#cholo2 = cholo.isel( z=0, x=slice(x1,x2), y=slice(y1,y2) )

#print( cholo2 )

out_schema = hp.core.detector_grid( shape=npix_out, spacing=cam_spacing/mag ) # set output shape

recons = ps_propagate( cholo, zstack, L, beam_c, out_schema ) # do propagation

pylab.figure(1)
pylab.imshow( np.absolute(recons.isel(z=0).values), cmap='gray' )
#for i in range(9):
#	pylab.subplot( 3, 3, i+1 )
#	pylab.imshow( np.absolute(recons.values[i]), cmap='gray' )
pylab.show()

