#!/usr/bin/python3
#
# HW settings required:
#   - Enable RPi-Camera
#   - set RPi "Memory Split" to 256MB
#

import os
import io
import time
import numpy
import imageio
import fractions
import picamera
import RPi.GPIO as GPIO
import xarray
import json

print( "APP BEGIN ..." )

#initialize GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)

#initialize parameters

# create the directory
path = "/home/pi/holo/pics/" + time.strftime("%Y-%m-%d_%H:%M:%S")
if not os.path.isdir( path ):
	os.makedirs(path)

# save the parameters
Parameters = {
    'objectName' : 'object',
    'cameraPinholeDistance' : 30.0,
    'cameraObjectDistance' : 5.91,
    'pinholeSize' : 0.15,
    'wavelength' : 0.43,
    'pixelSize' : 1.1,
    'mediumRefractionIndex': 1.0
}
open( path + "/parameters.json", "w" ).write( json.dumps(Parameters) )

#turn on LED and capture images

GPIO.output(17, GPIO.HIGH)  #turns on LED 

XRES0  = 3280
XRES   = 3296
YRES   = 2464
PXSIZE = 1.1

blueDataSum = numpy.zeros( (YRES, XRES), dtype='uint16' )
with picamera.PiCamera() as camera:
	
	# setup camera
	camera.resolution = (XRES0, YRES) # max resolution
	camera.framerate = 2 # 2fps is the max at max resolution
	camera.exposure_mode = 'off'
	camera.shutter_speed = int(50000) # 100ms is enough to make a max intensity picture in ISO100 level
	camera.iso = 100
	camera.awb_mode = 'off'
	camera.awb_gains = (1,1)
	camera.image_denoise = False
	camera.image_effect = 'none'

	# take pictures
	rgbBytesList = [ io.BytesIO() for i in range(10) ]
	camera.capture_sequence( rgbBytesList, format='rgb' )
	for i in range(len(rgbBytesList)):
		blueData= numpy.frombuffer( rgbBytesList[i].getbuffer(), dtype='uint8' ).reshape(YRES,XRES,3)[:,:,2]
		print( "APP blueData.max()=" + str(blueData.max()) )
		blueDataSum += blueData

	# release camera resources
	camera.close()

blueDataSumMax = blueDataSum.max()
if blueDataSumMax > 255:
    imgData = ( blueDataSum * 255.0 / blueDataSumMax ).astype('uint8')
else:
    imgData = blueDataSum.astype('uint8')
imageio.imsave( path + '/' + Parameters['objectName'] + '.jpg', imgData )
#imageio.imsave( path + '/' + Parameters['objectName'] + '.spi', blueDataSum, format='SPIDER' )

da = xarray.DataArray(
	numpy.array([blueDataSum]),
	dims=('z','y','x'),
	coords={ 'x':numpy.arange(0,XRES)*PXSIZE, 'y':numpy.arange(0,YRES)*PXSIZE, 'z':[0.0] },
	attrs={
                '_attr_coords' : '{illum_polarization: !!python/object/apply:collections.OrderedDict [[[vector, [x, y, z]]]], illum_wavelen: false, medium_index: false, noise_sd: false}',
		'medium_index' : Parameters['mediumRefractionIndex'],
		'illum_wavelen': Parameters['wavelength'],
		'illum_polarization': numpy.array([0.,0.,1.]), #xarray.DataArray( [1.,0.,0.], dims=('vector'), coords={'vector':['x','y','z']} ),
		'noise_sd' : 0.0,
                'name' : Parameters['objectName']
	},
        name='object'
)

print(da)

da.to_netcdf( path + '/' + Parameters['objectName'] + '.h5', engine='h5netcdf' )


GPIO.output(17, GPIO.LOW)   #turns off LED

GPIO.cleanup()

print( "APP END." )

