#!/usr/bin/python

import os
import sys
import pylab
import imageio
import numpy as np
import scipy.ndimage.measurements

outSubDir = sys.argv[1]

# ------ Read Input Directory -----

images = []

for fn in sorted(os.listdir(outSubDir), reverse=True):
	if ".png" not in fn: continue
	color = ''
	alpha = 0.7
	if "dust" in fn:
		color='k'
		alpha=1.0
	if "covid19" in fn:
		color='r'
		alpha=1.0
	images.append( [outSubDir + os.sep + fn, color, alpha] )

# read parameters
execfile( outSubDir + os.sep + "params.txt" )
STEP = float(PIXELSIZE) * DOWNSCALE_OUT


# ------ Load Images ------

N = len(images)

img = [None] * N

for i in range(N):
	fileName = images[i][0]
	img[i] = imageio.imread( fileName )
	print( "* " + fileName + " " + str(img[i].shape) )


# ------ Calculate Center of Mass -----

centerOfMass = [None] * N

for i in range(N):
	centerOfMass[i] = scipy.ndimage.measurements.center_of_mass( img[i] )
	print( "* " + images[i][0] + " " + str(img[i].shape) + " | " + str(centerOfMass[i]) )


# ------ Caluclate Radial Intensity Function ------

Center = img[0].shape[0] / 2.0
Rmax = img[0].shape[0] / 2
K = int( 2.0 * np.pi * Rmax )

vec = np.zeros( (N,Rmax) )

for r in range(Rmax):
	for k in range(K):
		angle = (2.0 * np.pi * k) / K
		x = int( Center + r * np.cos(angle) )
		y = int( Center + r * np.sin(angle) )
		for i in range(N):
			vec[i,r] += img[i][y,x]

vec /= Rmax * K


##### PLOT #####
plotLines = []
plotLabels = []

fig1 = pylab.figure(1)

ax1 = fig1.add_axes([0.1, 0.1, 0.8, 0.8])
ax1.set_ylabel('Normalized Intensity')
ax1.set_xlabel('Distance from Center (um)')
ax1.set_title('Simulated Hologram Stats (d='+str(OBJ_CAM_DISTANCE)+'um, w='+str(WAVELENGTH)+'um)')

for i in range(N):
	p, = pylab.plot( np.arange(0,STEP*len(vec[i]),STEP), vec[i], images[i][1], alpha=0.7 )
	plotLines.append(p)
	plotLabels.append( images[i][0].split(os.sep)[-1].split(".")[0] )

#pylab.figure(2)
#pylab.imshow( img[4] - img[0] )

pylab.legend( plotLines, plotLabels )

pylab.show()

