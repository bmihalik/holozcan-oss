#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# used sources:
#   http://kmdouglass.github.io/posts/simulating-inline-holograms/
#   https://arxiv.org/abs/1412.3674
#
#   Tatiana Latychevskaia, Hans-Werner Fink
#   Practical algorithms for simulation and reconstruction of digital in-line holograms
#   2016 * Physics Department, University of Zurich
#   (algorithms in article are based on Fresnel-Kirchoff diffraction integral
#    and Angular Spectrum Method by  J. A. Ratcliffe, the later is used in this code)
#

import os
import sys
import numpy as np
from numpy.fft import fft2, fftshift, ifftshift, ifft2

import imageio
import PIL.PngImagePlugin
import argparse


parser = argparse.ArgumentParser( description='Hologram simulator' )

parser.add_argument('--input-file', dest='InputFileName', help='input file path', metavar='FILE', default="<draw:circle(512,40,0.9,0.75)>" )
parser.add_argument('--output-file', dest='OutputFileName', help='output file path', metavar='FILE', default='hologram.png' )
parser.add_argument('--holosize', dest='SideSize', help='side size of hologram in pixels', metavar='NUM', type=int, default=1024 )
parser.add_argument('--pixelsize', dest='PixelSize', help='pixel size in micrometers', metavar='FLOAT', type=float, default=1.1 )
parser.add_argument('--downscale-out', dest='DownScaledOut', help='dowscaling ratio of output image (for ex. 10 means 10x smaller pic)', metavar='NUM', type=int, default=1 )
parser.add_argument('--downscale-in', dest='DownScaledIn', help='dowscaling ratio of input image', metavar='NUM', type=int, default=1 )
parser.add_argument('--normalize', dest='Normalize', help='normalize output image intensities', action='store_true', default=False )
parser.add_argument('--wavelength', dest='WaveLength', help='wavelength of light', metavar='FLOAT', type=float, default=0.43 )
parser.add_argument('--distance', dest='ObjCamDistance', help='distance of object and camera', metavar='FLOAT', type=float, default=1.0 )

options = parser.parse_args()


Fs         = 1.0 / options.PixelSize # Spatial sampling frequency [1/um]
FsVec      = np.arange( -Fs/2, Fs/2, step = Fs/options.SideSize ) # Spatial sampling frequency vector [1/um]
FX, FY     = np.meshgrid(FsVec, FsVec) # Spatial sampling frequency matrices [1/um]

# create clean planar light field (incident wave)
Obj = np.ones((options.SideSize, options.SideSize), dtype=np.complex)


# aAmplitude - [0..1]
# aPhase - [radians]
def create_circle_obj( aCenter, aRadius, aAmplitude, aPhase ):
	global Obj
	PosMatX, PosMatY = np.meshgrid(np.arange(0, options.SideSize), np.arange(0, options.SideSize))
	ObjMask = np.sqrt((PosMatX - aCenter)**2 + (PosMatY - aCenter)**2) <= aRadius
	Obj[ObjMask] = aAmplitude * np.exp(1j * aPhase) # put object into the clean light field


# a - 2D array
# d - downsampling factor
def downscale( a, d ):
	b = np.zeros( (a.shape[0]/d, a.shape[1]/d) )
	for y in range(b.shape[0]):
		for x in range(b.shape[1]):
			b[y,x] = a[ y*d:(y+1)*d, x*d:(x+1)*d ].mean()
	return b


#
# loads RGB image, B channel is amplitude (0..255 = 0..1), G channel is phase shift (255 = 2PI radian)
#
def create_obj_from_image( aFileName ):
	global Obj
	img = imageio.imread( aFileName )
	imgAmpli = img[:,:,2] # blue channel
	imgPhase = img[:,:,1] # green channel
	if options.DownScaledIn > 1:
		imgAmpli = downscale( imgAmpli, options.DownScaledIn )
		imgPhase = downscale( imgPhase, options.DownScaledIn )
	sizeX  = imgAmpli.shape[1]
	sizeY  = imgAmpli.shape[0]
	startX = int(options.SideSize/2 - sizeX/2)
	startY = int(options.SideSize/2 - sizeY/2)
	for x in range(sizeY):
		for y in range(sizeX):
			ampli = imgAmpli[y,x] / 255.0
			phase = 2 * np.pi * imgPhase[y,x] / 255.0
			Obj[startY+y,startX+x] = ampli * np.exp(1j * phase)
	print( "Object Image Loaded." )


# S(u,v) = exp( 2 * pi * i * z / lambda * sqrt( 1 - (lambda*u)^2 - (lambda*v)^2 ) )
def angular_spectrum_method_factors( u, v, z, wl ):
	square_root = np.sqrt( 1 - (wl**2 * u**2) - (v**2 * fy**2) )
	temp = np.exp( 1j * 2 * np.pi * z / wl * square_root )
	temp[np.isnan(temp)] = 0 # replace nan's with zeros
	return temp

# S(u,v) = exp( -i * pi * lamda * z * (u^2 + v^2) )
def fresnel_gabor_paraxial_approx_factors( u, v, z, wl ):
	return np.exp( - 1j * np.pi * wl * z * (u**2 + v**2) )

def propagate_lightfield( field, z, wl ):
	field_spectrum = ifftshift(fft2(fftshift(field))) * options.PixelSize**2
	#field_spectrum *= angular_spectrum_method_factors( FX, FY, z, wl )
	field_spectrum *= fresnel_gabor_paraxial_approx_factors( FX, FY, z, wl )
	return fftshift(ifft2(ifftshift(field_spectrum))) / options.PixelSize**2


def lightfield_to_hologram( field ):
	return np.abs(field)**2


print( "BEGIN." )

if options.InputFileName[0:6] == "<draw:":
	if options.InputFileName[6:13] == "circle(":
		beginPos = 13
		endPos = options.InputFileName[beginPos:].find(')')
		paramStr = options.InputFileName[beginPos:beginPos+endPos]
		params = [ float(x) for x in paramStr.split(",") ]
		create_circle_obj( params[0], params[1], params[2], params[3] )
else:
	create_obj_from_image( options.InputFileName )

f1 = propagate_lightfield( Obj, options.ObjCamDistance, options.WaveLength )
hologram = lightfield_to_hologram( f1 )

if options.DownScaledOut > 1:
	hologram = downscale( hologram, options.DownScaledOut )

RawOutputMin = hologram.min()
RawOutputMax = hologram.max()

if options.Normalize:
	img = (0xff*(hologram-RawOutputMin)/(RawOutputMax-RawOutputMin)).astype('uint8')
else:
	img = (0xff*hologram/RawOutputMax).astype('uint8')

imgInfo = PIL.PngImagePlugin.PngInfo()
imgInfo.add_text( "Creator", "holozcan-sim.py" )
imgInfo.add_text( "Input", options.InputFileName )
imgInfo.add_text( "Output", options.OutputFileName )
imgInfo.add_text( "RawOutputMin", "%f" % RawOutputMin )
imgInfo.add_text( "RawOutputMax", "%f" % RawOutputMax )
imgInfo.add_text( "Wavelength", "%0.0f nm" % (options.WaveLength * 1000.0) )
imgInfo.add_text( "HoloSize", "%d px" % options.SideSize )
imgInfo.add_text( "PixelSize", "%f um" % (options.PixelSize * options.DownScaledOut) )
imgInfo.add_text( "ObjCamDistance", "%0.0f um" % options.ObjCamDistance )

print( "PNGInfo:" )
for item in imgInfo.chunks:
	kv = item[1].split('\0')
	print( "  " + kv[0] + " = " + kv[1] )

imageio.imwrite( options.OutputFileName, img, pnginfo=imgInfo )

print( "END." )

