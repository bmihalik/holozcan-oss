#!/bin/bash

HOLOSIZE=8192
PIXELSIZE=0.001
WAVELENGTH=0.43
OBJ_CAM_DISTANCE=1.0
DOWNSCALE_IN=1
DOWNSCALE_OUT=10

#INPUT_OBJS="sphere-74nm sphere-86nm sphere-126nm dust-86nm corona-117nm"
#OUTSUBDIR=005

#INPUT_OBJS="sphere-126nm-117ri sphere-126nm-168ri sphere-126nm-220ri sphere-126nm-271ri sphere-126nm-322ri sphere-126nm-374ri"
#OUTSUBDIR=006

INPUT_OBJS="sphere-74nm sphere-86nm sphere-126nm dust-86nm corona-117nm"
OUTSUBDIR=007
WAVELENGTH=0.76


[ -d "output/${OUTSUBDIR}" ] || mkdir -p "output/${OUTSUBDIR}"

PARAMFILE="output/${OUTSUBDIR}/params.txt"
echo >${PARAMFILE} 
echo "HOLOSIZE=${HOLOSIZE}" >> ${PARAMFILE}
echo "PIXELSIZE=${PIXELSIZE}" >> ${PARAMFILE}
echo "WAVELENGTH=${WAVELENGTH}" >> ${PARAMFILE}
echo "OBJ_CAM_DISTANCE=${OBJ_CAM_DISTANCE}" >> ${PARAMFILE}
echo "DOWNSCALE_OUT=${DOWNSCALE_OUT}" >> ${PARAMFILE}
echo "DOWNSCALE_IN=${DOWNSCALE_IN}" >> ${PARAMFILE}

for name in ${INPUT_OBJS}
do
	echo "***** ${name} *****"
	./holozcan-sim.py \
		--input-file=../../resources/simu-input/${name}.png \
		--output-file=output/${OUTSUBDIR}/${name}.png \
		--wavelength=${WAVELENGTH} \
		--holosize=${HOLOSIZE} \
		--pixelsize=${PIXELSIZE} \
		--distance=${OBJ_CAM_DISTANCE} \
		--normalize \
		--downscale-out=${DOWNSCALE_OUT} \
		--downscale-in=${DOWNSCALE_IN}
done

